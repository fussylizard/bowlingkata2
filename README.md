# README #
This project is a starting point for doing the [Bowling Game Kata](http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata) Test-Driven Development (TDD) exercise in PHP.  If you have never done TDD before, it's a great way to get a feel for how TDD works.

Use this simple skeleton project as a starting point.  Verify you can run the sample test to validate your environment and then start developing the bowling score calculator code using the tests and steps outlined in the exercise.  Note the slides use Java, so you'll have to write yours in PHP.

### How do I get set up? ###
* Verify you have Composer installed and configured.
    * Run `composer` from the command line and verify you get Composer's help.
    * Install from https://getcomposer.org/ if you need it.
* Verify you have PHPUnit installed and configured globally.
    * Run `phpunit` from your command line and verify you get PHPUnit's help.
    * Install from https://phpunit.de if you need it.
* Download this repository
* Run composer to create the autoloads
    * Go to your project's root directory
    * Run `composer install`
* Verify you can run the starter test
    * Go to your project's root directory
    * Run `phpunit` and verify the output says one test ran successfully
* (Optional) Setting up the project in PHPStorm
    * Once you've followed the above steps, create a new PHPStorm project using the "Create a project from existing files" option
    * Set the PHP interpreter via PHP Storm Preferences > PHP
    * Set the path to phpunit.phar via PHP Storm Preferences > PHP > PHPUnit.  Note if you only have a phpunit file, you should just make a copy of it to have a phpunit.phar file and point to that.
    * Create a new run configuration based on the default "PHPUnit" configuration with the following options:
        * Set Test Scope to “Defined in the configuration file”
        * Tick the box for "Use alternative configuration file" and set it to the phpunit.xml file in the project
    * Save your new run configuration
    * You should now be able to run your unit tests via the run configuration within the PHPStorm GUI


### To Do ###
* Would be nice to convert the slides into PHP code

### Who do I talk to? ###

* Contact chris_durand@bridge360.com for more information!

### Appreciations ###
* A huge thank you to Justin Tilson for helping me get things cleaned up into a proper PHP style project and getting things running smoothly in PHPStorm!  Thanks Justin!